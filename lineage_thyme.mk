#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from thyme device
$(call inherit-product, device/xiaomi/thyme/device.mk)

# Inherit MindTheGapps
$(call inherit-product, vendor/gapps/arm64/arm64-vendor.mk)

PRODUCT_NAME := lineage_thyme
PRODUCT_DEVICE := thyme
PRODUCT_MANUFACTURER := xiaomi
PRODUCT_BRAND := Xiaomi
PRODUCT_MODEL := M2102J2SC

PRODUCT_CHARACTERISTICS := nosdcard

PRODUCT_GMS_CLIENTID_BASE := android-xiaomi

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="thyme-user 13 RKQ1.211001.001 V14.0.6.0.TGACNXM release-keys"

BUILD_FINGERPRINT := Xiaomi/thyme/thyme:13/RKQ1.211001.001/V14.0.6.0.TGACNXM:user/release-keys
